/* Requiring necessary packages */
var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	browsersync = require('browser-sync'),
	changed = require('gulp-changed'),
	concat = require('gulp-concat'),
	cssnano = require('gulp-cssnano'),
	uglify = require('gulp-uglify'),
	tinypng = require('gulp-tinypng-compress'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps');

/* Setting base project constants */
const paths = {
	src: './src/',
	dest: './app/'
};

/* Setting an error swallower */
var swallowError = function(error) {
	console.log(error.toString())
	this.emit('end')
}

/*
* BASIC
*
* Those listed below are the basic tasks
* for compiling & distributing files
*/
gulp.task('html', function() {
	gulp.src([paths.src + '*.html'])
	.pipe(changed(paths.dest))
	.pipe(gulp.dest(paths.dest));
});

gulp.task('css', function() {
	gulp.src([paths.src + 'scss/*.scss'])
	.pipe(changed(paths.dest))
	.pipe(sass({includePaths:['scss/**']}))
	.on('error', swallowError)
	.pipe(autoprefixer())
	.pipe(cssnano({zindex: false}))
	.pipe(concat('style.css'))
	.pipe(gulp.dest(paths.dest + 'css'));
});

gulp.task('js', function() {
	gulp.src([paths.src + 'js/**/*.js'])
	.pipe(changed(paths.dest + 'js'))
	.pipe(uglify())
	.pipe(concat('scripts.min.js'))
	.pipe(gulp.dest(paths.dest + 'js'));
});

gulp.task('img', function() {
	// Setting allowed images
	gulp.src([
		paths.src + 'img/*.jpg',
		paths.src + 'img/*.png',
		])
	.pipe(changed(paths.dest + 'img'))
	.pipe(tinypng({
			key: 'znQaEIjGEHt8nKJc5cXHAkOOxMbCnWf4',
			sigFile: 'images/.tinypng-sigs',
			log: false
		}))
	.pipe(gulp.dest(paths.dest + 'img'));
});

gulp.task('libs', function() {
	/* 
	* Here comes all the third-party files
	* like Fontawesome, bulma...
	*/

	// CSS Libs
	gulp.src([
		'node_modules/normalize.css/normalize.css',
		])
	.pipe(changed(paths.dest + 'css'))
	.pipe(sass())
	.pipe(autoprefixer())
	.pipe(cssnano())
	.pipe(concat('libs.min.css'))
	.pipe(gulp.dest(paths.dest + 'css'));

	// JS Libs
	gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/slick-carousel/slick/slick.min.js',
		])
	.pipe(changed(paths.dest + 'js/libs'))
	.pipe(concat('libs.min.js'))
	.pipe(gulp.dest(paths.dest + 'js/libs'));
});

gulp.task('watch', function() {
	var html = gulp.watch([paths.src + '*.html'], ['html']),
	css = gulp.watch([paths.src + 'scss/*.scss'], ['css']),
	js = gulp.watch([paths.src + 'js/*.js'], ['js']);

	browsersync.init([paths.dest], {
		browser: 'chrome',
		server: {
		    baseDir: "app",
		    index: "index.html"
		},
		port: 3000,
		notify: false
	});
});


/*
* GLOBAL
*
* This task runs everything in basic
* task list, except "Deploy task"
*/
gulp.task('default', ['html', 'css', 'js', 'img', 'libs', 'watch']);