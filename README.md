### Como usar? ###

Esse repositório apresenta todas as dependências do er outras coisas nos elementos do hotsite.

### Como configurar o Gulp em minha máquina? ###

Antes de tudo você vai precisar instalar alguns componentes na sua máquina para que consiga executar o gulp sem problemas. Os componentes são:

* Node.js - [https://nodejs.org/en/](Link URL)
* Gulp

Abra o seu terminal (Git Bash de preferência) e execute os seguintes comandos na ordem que são apresentados:

```
#!shellscript

npm install gulp -g

cd just-digital/app

npm install
gulp
```

O último comando será o responsável por executar a tarefa *Default* do *Gulpfile.js* presente dentro app. Ele iniciará o server e automaticamente abrirá o *localhost:3000* como URL.

**Obs.: Não se esqueça de iniciar seu XAMPP/MAMP/WAMP antes de executar o comando Gulp.**