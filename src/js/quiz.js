

window.addEventListener("load", function() {
  loadActor();

  var next = document.querySelector("#next");
  var form = document.querySelector("#form");
  var answerName = document.querySelector("#your-answer");
  var giveUp = document.querySelector("#giveup");

//loading actors name and photo
function loadActor(){
  var xhr = new XMLHttpRequest();
  var actorPhoto = document.querySelector("#photo");
  var loading = document.querySelector("#loading");

  loading.classList.remove("hide");

  xhr.open("GET","http://www.theimdbapi.org/api/person?person_id=nm"+magicNumber(5000,1))
  xhr.addEventListener("load",function(){

    var imdb = xhr.responseText;
    var actor = JSON.parse(imdb);    

    if (actor.photos == 0) {
      loadActor()
    } else if (xhr.status == 200) {
      loading.classList.add("hide");
      console.log(this);
      console.log(actor);
      actorPhoto.src = actor.photos[0];
      window.actor = actor.title;
    }
  });
  xhr.send();
}

//check if actors name matches
function nameMatch(name){
  var answer = name.toLowerCase();
  var actorName = document.querySelector("#name");
  
  if (answer === actor.toLowerCase() && answerName.classList.contains("tip")) {
      message("tip");
  } else if(answer === actor.toLowerCase()) {
      message("right");
  } else {
    message("wrong");
  }

}

//set score
function score(status){
  var scored = document.querySelector("#score");
  var score = scored.innerHTML;

  if(status == "right"){
    score++;
    scored.innerHTML = score;
    reset(answerName);
    setTimeout(function(){next.click();}, 500);

    if (window.scoreRight == undefined ) {
      scoreRight = 0;
      window.scoreRight++;
    } else {
      window.scoreRight++;
    }   

  } else if (status == "wrong") {
      score--;
    answerName.classList.remove("tip");
    scored.innerHTML = score;
    reset(answerName);
    setTimeout(function(){next.click();}, 500);

    if (window.scoreWrong == undefined ) {
      scoreWrong = 0;
      window.scoreWrong++;
    } else {
      window.scoreWrong++;
    }   

  } else if (status == "tip"){
    answerName.classList.remove("tip");
    reset(answerName);    
    setTimeout(function(){next.click();}, 500);

     if (window.scoreTip == undefined ) {
      scoreTip = 0;
      window.scoreTip++;
    } else {
      window.scoreTip++;
    }   
  }
  countScore();
}

function countScore(){
  
  window.total = (scoreRight + scoreWrong + scoreTip);
  if (total == 10) {
    message('theend');
  }
}

// reset form field
function reset(field){
  field.value = "";
}
//magic number to get random actor
function magicNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

//show actor"s name when give up
function showActor(){
  var caption = document.querySelector("#caption");
  var actorName = document.querySelector("#name");

  actorName.innerHTML = actor;
  caption.classList.add("show");
  setTimeout(function(){
    caption.classList.remove("show");
    actorName.innerHTML = "";
  }, 2000);
}

// message
function message(status) {
  var message = document.querySelector("#message");
  var messageTitle = document.querySelector("#message-title");
  var messageText = document.querySelector("#message-text");

  if (status == "right") {
    message.classList.remove("hide");

    messageTitle.innerHTML = "Congratulations";
    messageText.innerHTML = "You got it!";

    setTimeout(function(){
      message.classList.add("hide");
      score("right");
    }, 3000);

  } else if (status == "wrong") {
    message.classList.remove("hide");

    messageTitle.innerHTML = "Sorry =(";
    messageText.innerHTML = "Your answer is wrong, the right answer is " + actor;

     setTimeout(function(){
      message.classList.add("hide");
      score("wrong");
    }, 3000);

  } else if (status == "tip"){
    message.classList.remove("hide");

    messageTitle.innerHTML = "Congratulations!";
    messageText.innerHTML = "You got it! But you used a tip, so you don't score =/";
    
     setTimeout(function(){
      message.classList.add("hide");
      score("tip");
    }, 3000);

  } else if (status == "theend") {
    message.classList.remove("hide");

    var scored = document.querySelector("#score");
    var scoreTotal = scored.innerHTML;
    // window.total = (qr + qw + qt);

    messageTitle.innerHTML = "Your results!";
    messageText.innerHTML = "You scored " + scoreTotal + " out of 10";
  }

}


next.addEventListener("click",function(e){
  e.preventDefault();
    loadActor();
});

form.addEventListener("submit",function(e){
  e.preventDefault();
  nameMatch(answerName.value);
});

giveUp.addEventListener("click", function(e){
  e.preventDefault();
  showActor();
  answerName.classList.add("tip");
});

})


